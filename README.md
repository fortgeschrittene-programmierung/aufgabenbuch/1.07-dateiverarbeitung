## 1.10 Dateiverarbeitung

**Voraussetzung:** Vorlesung zum Thema I/O.

**Ziel:** Übung der Dateiverarbeitung auf Byte- und Zeichenbasis.

**Dauer:** < 45 Minuten

## Aufgabe
Erstellen Sie eine Textdatei namens `numbers.txt` mit einem Texteditor und dem folgenden Inhalt:

```
20.0
39.4
90.3
32.4
45.3
87.4
39.5
49.9
39.9
72.5
100.5
3.5
4.0
19
47
58.4
```

Die Datei enthält also pro Zeile eine Zahl. Schreiben Sie ein Programm, das die Datei einliest und dabei den Durchschnitt der eingelesenen Zahlen berechnet. Das Programm soll das Ergebnis in einer neuen Datei namens `average.txt` speichern.

**Hinweis:** Sollte Ihre `numbers.txt` nicht vom Programm gefunden werden, prüfen Sie, ob die Datei im Projektordner gespeichert ist. Alternativ können Sie auch den absoluten Pfad zur Datei im Code verwenden.
